--Bloque anonimo para copiar las tablas de un usuario a otro
--Nota: No copia pk ni constraints, solo estructura y datos

BEGIN 
    FOR c IN (SELECT * FROM USER_TABLES) LOOP
        EXECUTE IMMEDIATE 'CREATE TABLE Luis2.'||c.TABLE_NAME||' AS SELECT * FROM HR.'||c.TABLE_NAME;
    END LOOP;
END;