--Funcion para calcular el promedio de horas de entrada dado el rfc, una fecha de inicio y una fecha de fin 

create or replace FUNCTION F_PROM_HE (RFC2 IN REGISTROEYS.RFC%TYPE, FECHAI IN REGISTROEYS.FECHA%TYPE, FECHAF IN REGISTROEYS.FECHA%TYPE)
    RETURN INTERVAL DAY TO SECOND
    IS
        prom_entrada registroeys.hora_entrada%type;
    BEGIN
        SELECT NUMTODSINTERVAL(AVG(EXTRACT(DAY FROM HORA_ENTRADA*24*60*60)),'SECOND') INTO prom_entrada --numdsinterval convierte la cantidad de segundos a formato de horas
        FROM REGISTROEYS WHERE RFC=RFC2 AND (FECHA BETWEEN FECHAI AND FECHAF);
        RETURN (prom_entrada);
    END;