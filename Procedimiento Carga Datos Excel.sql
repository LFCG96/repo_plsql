--SP para cargar datos de la tabla temporal con datos de excel a la tabla real

create or replace PROCEDURE SP_INSERTAR_DATOS_EXCEL 
is
   V_FECHAT DATE;
   V_DESCRIPCIONT VARCHAR2(20 BYTE);
   exito number(5,0);
   fracaso number(5,0);
   bandera number(5,0);

   CURSOR CURSOR1 is select * from temp_diafestivo; --cursor para la tabla temporal
   begin
    exito:=0;
    fracaso:=0;
    bandera:=1;
   OPEN CURSOR1;
   LOOP 
       FETCH CURSOR1 INTO V_FECHAT, V_DESCRIPCIONT; --recorremos el cursor con las variables
       exit when CURSOR1%NOTFOUND;   --sale del loop si ya no hay datos

        for c in (select * from diafestivo) loop
            if c.fecha = V_FECHAT then --valida que no exista el regitro dentro de la tabla original
                bandera:=0; --variable de control para insertar o no un registro
                fracaso:=fracaso+1; --contador de registros no insertados
                DBMS_OUTPUT.put_line('Registro no v�lido, llave duplicada: '||v_fechat||': '||v_descripciont);
            end if;
        end loop;
      if bandera=1 then  --si no existe en la tabla original validamos los campos
          if V_FECHAT is null or V_DESCRIPCIONT is null then 
              fracaso:=fracaso+1;
              DBMS_OUTPUT.put_line('Registro no v�lido, datos nulos');
          else 
              INSERT INTO DIAFESTIVO VALUES(V_FECHAT, V_DESCRIPCIONT); --inserta en la tabla original :D
              exito:=exito+1;
              delete temp_diafestivo where fechat=v_fechat; --borramos de la tabla temporal
          end if;
      end if;
      bandera:=1;

   END LOOP; --imprimimos los resultados contados
  DBMS_OUTPUT.put_line('Registros no insertados '||to_char(fracaso));
  DBMS_OUTPUT.put_line('Registros insertados exitosamente '||to_char(exito));
  DBMS_OUTPUT.put_line('Consulte la tabla temp_diafestivo para verificar los errores');
  EXCEPTION
        WHEN OTHERS THEN
        DBMS_OUTPUT.put_line('Error al ejecutar el procedimiento');
 END;