--Cursores anidados
--*******************************************
declare 
    CURSOR cur_dept IS
        SELECT department_id, department_name
            FROM departments
            ORDER BY department_name;
    CURSOR cur_emp (p_deptid NUMBER) IS
        SELECT first_name, last_name
            FROM employees
            WHERE department_id = p_deptid
            ORDER BY last_name;
    v_deptrec   cur_dept%rowtype;
    v_emprec    cur_emp%rowtype;
BEGIN
    OPEN cur_dept;
    LOOP
        FETCH cur_dept INTO v_deptrec; --Muestra el departamento y sus trabajadores :D 
        EXIT WHEN cur_dept%notfound;
        dbms_output.put_line(v_deptrec.department_name);
        OPEN cur_emp(v_deptrec.department_id);
        LOOP
            FETCH cur_emp INTO v_emprec;
            EXIT WHEN cur_emp%notfound;
            dbms_output.put_line(v_emprec.last_name
                                 || ' '
                                 || v_emprec.first_name);
        END LOOP;
        CLOSE cur_emp;
    END LOOP;
    CLOSE cur_dept;
END;

-- hace lo mismo pero con for loops 
DECLARE
    CURSOR cur_dept IS SELECT * FROM departments;
    CURSOR cur_emp (p_dept_id NUMBER) IS
        SELECT * FROM employees WHERE department_id = p_dept_id
        FOR UPDATE NOWAIT;
BEGIN
    FOR v_deptrec IN cur_dept LOOP
        DBMS_OUTPUT.PUT_LINE(v_deptrec.department_name||':');
        FOR v_emprec IN cur_emp (v_deptrec.department_id) LOOP
            DBMS_OUTPUT.PUT_LINE(v_emprec.last_name);
            IF v_deptrec.location_id = 1700 AND v_emprec.salary < 10000 --y actualiza el salario de esta condici�n
                THEN UPDATE employees
                        SET salary = salary * 1.1
                        WHERE CURRENT OF cur_emp;
            END IF;
        END LOOP;
    END LOOP;
END;

--USO DE RECORDS
--******************************************************+
DECLARE
    v_emp_record employees%ROWTYPE; --guarda el tipo de dato de row de empleados
BEGIN
    SELECT * INTO v_emp_record
        FROM employees
        WHERE employee_id = 100;
    DBMS_OUTPUT.PUT_LINE('Email for ' || v_emp_record.first_name ||
    ' ' || v_emp_record.last_name || ' is ' || v_emp_record.email ||
    '@oracle.com.');
END;

--Un record basado en otro record
DECLARE
    v_emp_record employees%ROWTYPE;
    v_emp_copy_record v_emp_record%ROWTYPE; --se basa en el otro record
BEGIN
    SELECT * INTO v_emp_record
        FROM employees
        WHERE employee_id = 100;
    v_emp_copy_record := v_emp_record;
    v_emp_copy_record.salary := v_emp_record.salary * 1.2; --Actualiza el salario de la copia del record
    DBMS_OUTPUT.PUT_LINE(v_emp_record.first_name ||
    ' ' || v_emp_record.last_name || ': Old Salary - ' ||
    v_emp_record.salary || ', Proposed New Salary - ' || 
    v_emp_copy_record.salary || '.');
END;--para proponer un nuevo salario

--Definiendo nuestros propios records (Es �til para cuando hacemos joins de tablas)
DECLARE
    TYPE person_dept IS RECORD --Tipo como record
        (first_name employees.first_name%TYPE,
        last_name employees.last_name%TYPE,
        department_name departments.department_name%TYPE);--Los campos que contiene
    v_person_dept_rec person_dept; --le asigno el tipo del record a la variable
BEGIN
    SELECT e.first_name, e.last_name, d.department_name
        INTO v_person_dept_rec
        FROM employees e JOIN departments d
        ON e.department_id = d.department_id
        WHERE employee_id = 200;
    DBMS_OUTPUT.PUT_LINE(v_person_dept_rec.first_name ||
    ' ' || v_person_dept_rec.last_name || ' is in the ' ||
    v_person_dept_rec.department_name || ' department.');
END; --Imprime el nombre del id 200 y el nombre del departamento al que pertenece

--RECORDS COMO INDEX OF TABLE 
--*************************************
DECLARE
    TYPE t_hire_date IS TABLE OF employees.hire_date%TYPE --tipo como tabla del tipo hire_date
        INDEX BY BINARY_INTEGER;
    v_hire_date_tab t_hire_date; --variable del tipo creado
BEGIN
    FOR emp_rec IN (SELECT employee_id, hire_date FROM employees)LOOP
        v_hire_date_tab(emp_rec.employee_id) := emp_rec.hire_date; --clave - valor
        DBMS_OUTPUT.PUT_LINE(v_hire_date_tab(emp_rec.employee_id)); --le das la clave y te imprime el valor
    END LOOP;
END;

--usando las funciones de index of table....
DECLARE
    TYPE t_hire_date IS TABLE OF employees.hire_date%TYPE
        INDEX BY BINARY_INTEGER;
    v_hire_date_tab t_hire_date;
    v_hire_date_count NUMBER(4);
BEGIN
    FOR emp_rec IN (SELECT employee_id, hire_date FROM employees)LOOP
        v_hire_date_tab(emp_rec.employee_id):= emp_rec.hire_date;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE(v_hire_date_tab.COUNT); --Cuenta la cantidad de registros en la variable tipo tabla
END;

--index of table de tipo record
DECLARE
    TYPE t_emp_rec IS TABLE OF employees%ROWTYPE
        INDEX BY BINARY_INTEGER;
    v_emp_rec_tab t_emp_rec;
BEGIN
    FOR emp_rec IN (SELECT * FROM employees) LOOP
        v_emp_rec_tab(emp_rec.employee_id) := emp_rec;
        DBMS_OUTPUT.PUT_LINE(v_emp_rec_tab(emp_rec.employee_id).salary); --imprime el salario de cada row del index of table 
    END LOOP;
END;

--********************EXCEPCIONES******************************
--*************************************************************

DECLARE
    v_country_name countries.country_name%TYPE := 'Korea, South';
    v_reg_id countries.region_id%TYPE;
BEGIN
    SELECT region_id INTO v_reg_id
        FROM countries WHERE country_name = v_country_name;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN --Si el registro no tiene datos
        DBMS_OUTPUT.PUT_LINE ('Country name, ' || v_country_name || ',
        cannot be found. Re-enter the country name using the correct
        spelling.');
END;

DECLARE
    v_lname VARCHAR2(15);
BEGIN
    SELECT last_name INTO v_lname
        FROM employees WHERE job_id = 'ST_CLERK';
    DBMS_OUTPUT.PUT_LINE('The last name of the ST_CLERK is : '||v_lname);
EXCEPTION
    WHEN TOO_MANY_ROWS THEN --Si hay muchas lineas retornadas y solo damos tratamiento para una....
        DBMS_OUTPUT.PUT_LINE ('Your select statement retrieved multiple rows.
        Consider using a cursor.');
    WHEN NO_DATA_FOUND THEN --Si el registro no tiene datos
        DBMS_OUTPUT.PUT_LINE ('NOT DATA FOUND.');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('Another type of error occurred');
END; --este codigo tiene multiples bloques de excepci�n D:

--Nombrar las excepciones de ORACLE que no est�n definidas con un nombre
DECLARE
    e_insert_excep EXCEPTION;
        PRAGMA EXCEPTION_INIT(e_insert_excep, -01400); --asignamos el codigo
BEGIN
    INSERT INTO departments
        (department_id, department_name)
        VALUES (280, NULL);
    EXCEPTION
        WHEN e_insert_excep THEN
            DBMS_OUTPUT.PUT_LINE('INSERT FAILED');
END;

DECLARE
    v_error_code NUMBER;
    v_error_message VARCHAR2(255);
BEGIN 
    --...
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        v_error_code := SQLCODE;
        v_error_message := SQLERRM; --son variables predefinidas por ORACLE
        INSERT INTO error_log(e_user, e_date, error_code, error_message)
        VALUES(USER, SYSDATE, v_error_code, v_error_message);
END; --aqui se muestra otra forma de manejar excepciones

--USER DEFINED EXCEPTIONS
DECLARE
    e_invalid_department EXCEPTION;
    v_name VARCHAR2(20):='Accounting';
    v_deptno NUMBER := 27;
BEGIN
    UPDATE departments
        SET department_name = v_name
        WHERE department_id = v_deptno;
    IF SQL%NOTFOUND THEN
        RAISE e_invalid_department; --lanzamos una excepcion pero no le asignamos un codigo
    END IF;
EXCEPTION
    WHEN e_invalid_department THEN 
        DBMS_OUTPUT.PUT_LINE('No such department id.');
END;

DECLARE
    v_mgr PLS_INTEGER := 123;
BEGIN
    DELETE FROM employees
        WHERE manager_id = v_mgr;
    IF SQL%NOTFOUND THEN
        RAISE_APPLICATION_ERROR(-20202,
        'This is not a valid manager');--aqui damos codigo y mensaje de error
    END IF;
END;

DECLARE --en este ejemplo lanzamos las excepciones desde la zona de EXCEPTION
    v_mgr PLS_INTEGER := 27;
    v_employee_id employees.employee_id%TYPE;
BEGIN
    SELECT employee_id INTO v_employee_id
        FROM employees
        WHERE manager_id = v_mgr;
    DBMS_OUTPUT.PUT_LINE('Employee #' || v_employee_id ||
    ' works for manager #' || v_mgr || '.');
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20201,
        'This manager has no employees');
    WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR(-20202,
        'Too many employees were found.');
END;

--******************************************************************
--**********************PAQUETES SIN CUERPO************************
--******************************************************************
CREATE OR REPLACE PACKAGE global_consts IS
    mile_to_kilo CONSTANT NUMBER := 1.6093;
    kilo_to_mile CONSTANT NUMBER := 0.6214;
    yard_to_meter CONSTANT NUMBER := 0.9144;
    meter_to_yard CONSTANT NUMBER := 1.0936;
END global_consts;--NOS SIRVEN PARA DECLARAR VARIABLES GLOBALES :D

GRANT EXECUTE ON global_consts TO PUBLIC;

--PARA INVOCARLO....
DECLARE
    distance_in_miles NUMBER(5) := 5000;
    distance_in_kilo NUMBER(6,2);
BEGIN
    distance_in_kilo := distance_in_miles * global_consts.mile_to_kilo;
    DBMS_OUTPUT.PUT_LINE(distance_in_kilo);
END;

--Declaraci�n de un paquete
CREATE OR REPLACE PACKAGE taxes_pkg IS
    FUNCTION tax (p_value IN NUMBER) RETURN NUMBER;
END taxes_pkg;

--se define....
CREATE OR REPLACE PACKAGE BODY taxes_pkg IS
    FUNCTION tax (p_value IN NUMBER) RETURN NUMBER IS
        v_rate NUMBER := 0.08;
    BEGIN
        RETURN (p_value * v_rate);
    END tax;
END taxes_pkg;

--se usa...
SELECT taxes_pkg.tax(salary), salary, last_name
FROM employees;

--otro ejemplo para sacar n registros de una consulta
CREATE OR REPLACE PACKAGE curs_pkg IS
    CURSOR emp_curs IS SELECT employee_id FROM employees
                            ORDER BY employee_id;
    PROCEDURE open_curs;
    FUNCTION fetch_n_rows(n NUMBER := 1) RETURN BOOLEAN;
    PROCEDURE close_curs;
END curs_pkg;

CREATE OR REPLACE PACKAGE BODY curs_pkg IS
    PROCEDURE open_curs IS
        BEGIN
            IF NOT emp_curs%ISOPEN THEN OPEN emp_curs; END IF;
        END open_curs;
    FUNCTION fetch_n_rows(n NUMBER := 1) RETURN BOOLEAN IS
            emp_id employees.employee_id%TYPE;
        BEGIN
            FOR count IN 1 .. n LOOP
                FETCH emp_curs INTO emp_id;
                EXIT WHEN emp_curs%NOTFOUND;
                DBMS_OUTPUT.PUT_LINE('Id: ' ||(emp_id));
            END LOOP;
            RETURN emp_curs%FOUND;
        END fetch_n_rows;
    PROCEDURE close_curs IS 
        BEGIN
            IF emp_curs%ISOPEN THEN CLOSE emp_curs; END IF;
        END close_curs;
END curs_pkg;

DECLARE
    v_more_rows_exist BOOLEAN := TRUE;
    n NUMBER := 1;
BEGIN
    curs_pkg.open_curs; --1
    LOOP
        v_more_rows_exist := curs_pkg.fetch_n_rows(n); --2
        DBMS_OUTPUT.PUT_LINE('-------');
        n:=n+1;
        EXIT WHEN NOT v_more_rows_exist;
    END LOOP;
    curs_pkg.close_curs; --3
END;

--****************************************************
--************ORACLE SUPLIED PACKAGES*****************
--****************************************************

PROCEDURE read(dir VARCHAR2, filename VARCHAR2) IS
    file UTL_FILE.FILE_TYPE;
BEGIN
    IF NOT UTL_FILE.IS_OPEN(file) THEN
    file := UTL_FILE.FOPEN (dir, filename, 'r');
    END IF; ...
END read;