--SP para eliminar datos de la BD sin alterar la estructura de las tablas de manera permanente

create or replace PROCEDURE ELIMINAR_DATOS
IS
BEGIN --a ES UN APUNTADOR, R INDICA QUE HAY CONSTRAINTS DE FOREIGN KEY
  FOR a IN (SELECT table_name, constraint_name FROM user_constraints WHERE constraint_type = 'R')
  LOOP --EJECUTAMOS UN ALTER DE LA TABLA a Y LO DESHABILITAMOS
    EXECUTE IMMEDIATE ('alter table ' || a.table_name || ' disable constraint ' || a.constraint_name);
  END LOOP;--EN LAS TABLAS DE USUARIO....
  FOR a IN (SELECT table_name FROM user_tables)
  LOOP--...TRUNCAMOS TODAS LAS TABLAS
    EXECUTE IMMEDIATE ('truncate table ' || a.table_name);
  END LOOP;
  FOR a IN (SELECT table_name, constraint_name FROM user_constraints WHERE constraint_type = 'R')
  LOOP--POR ULTIMO VOLVEMOS A HABILITAR LOS CONSTRAINTS 
    EXECUTE IMMEDIATE ('alter table ' || a.table_name || ' enable constraint ' || a.constraint_name);
  END LOOP;
END;