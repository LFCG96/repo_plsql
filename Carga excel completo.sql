--CREACI�N DE LOS DIRECTORIOS Y PERMISOS ESPECIALES PARA EL RESPALDO Y LA INSERCI�N DE DATOS

GRANT EXECUTE ON sys.UTL_FILE TO TCHECK;

CREATE DIRECTORY RESPALDOS AS 'C:\Users\silve\resp';
CREATE DIRECTORY BACKUPS  AS 'C:\Users\silve\backup';

grant read, write on directory RESPALDOS to TCHECK;
grant read, write on directory BACKUPS to TCHECK;

GRANT  EXP_FULL_DATABASE, IMP_FULL_DATABASE TO TCHECK;

select * from all_directories;
SELECT * FROM DBA_TAB_PRIVS WHERE TABLE_NAME='UTL_FILE';
--TABLA TEMPORAL----
create table temp_diafestivo (
        Fechat DATE NOT NULL PRIMARY KEY,
        Descripciont VARCHAR2(20) NOT NULL, 
        Estado CHAR(1),
        Comentario VARCHAR2(20)
);

--PROCEDIMIENTO PARA CARGAR DATOS A TABLA TEMPORAL--
create or replace PROCEDURE SP_DATOS_TEMPORAL
IS
   F UTL_FILE.FILE_TYPE;
   V_LINE VARCHAR2 (32767);
   V_FECHAT DATE;
   V_DESCRIPCIONT VARCHAR2(20 byte);
   V_ESTADO CHAR(1 byte);
   V_COMENTARIO VARCHAR2(20 byte);
  
   BEGIN
  
 F := UTL_FILE.FOPEN ('RESPALDOS', 'Festivos.csv', 'R'); --Directorio, Archivo.csv, Read
IF UTL_FILE.IS_OPEN(F) THEN
LOOP
BEGIN
UTL_FILE.GET_LINE(F, V_LINE, 32767); --Obtiene el contenido
IF V_LINE IS NULL THEN
EXIT;
END IF;
    V_FECHAT := REGEXP_SUBSTR(V_LINE, '[^,]+|,,|,\s+?,', 1, 1); --Separa cada campo por comas ',' para el campo v_fechat
    V_DESCRIPCIONT := REGEXP_SUBSTR(V_LINE, '[^,]+|,,|,\s+?,', 1, 2); --Lo mismo para el campo v_descripciont
     
     if  REGEXP_LIKE(v_fechat,',,|,\s+?,') then --filtra que no est�n vac�os
       v_fechat:= null;
    end if;
    if  REGEXP_LIKE(v_descripciont,',,|,\s+?,') then 
        v_descripciont:= null;
    end if;

   INSERT INTO temp_diafestivo (Fechat, Descripciont)VALUES(V_FECHAT, V_DESCRIPCIONT);  --Inserta los campos en la tabla temporal 
   
EXCEPTION
WHEN NO_DATA_FOUND THEN --Si no hay datos en el archivo
EXIT;
WHEN OTHERS THEN 
DBMS_OUTPUT.put_line('Oh, oh! Algo sali� mal');
END;
END LOOP;
END IF;
UTL_FILE.FCLOSE(F);
END;
--------------------------------------
SELECT * FROM temp_diafestivo;
select * from empleados;
EXEC SP_DATOS_TEMPORAL;




