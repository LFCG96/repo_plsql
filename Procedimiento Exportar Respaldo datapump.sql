--SP exportar bd usando el paquete dbms_datapump :D

create or replace procedure sp_respaldo
is
  ind NUMBER;              -- Indice para loop
  h2 NUMBER;               -- Data Pump job handle, "palanca" para manejar el job del respaldo
  percent_done NUMBER;     -- porentaje del trabajo realizado
  job_state VARCHAR2(30);  -- para dar seguimiento al estado del trabajo
  le ku$_LogEntry;         -- para WIP (Work in progress) y mensajes de error
  js ku$_JobStatus;        -- Estado de trabajo de get_status
  jd ku$_JobDesc;          -- Desripción del trabajo de get_status
  sts ku$_Status;          -- Objeto de status obtenido por get_status
BEGIN
-- Creamos un trabajo de Data Pump (Bomba de datos) para exportar el esquema
  h2 := DBMS_DATAPUMP.OPEN('EXPORT','SCHEMA',NULL,'RESPALDO','LATEST');
-- Especificar el nombre del archivo a crear y el directorio a utilizar
-- en el handle usado en la línea anterior
  DBMS_DATAPUMP.ADD_FILE(h2,'RESPALDO_TCHECK.dmp','BACKUPS');
-- Se usa un filtro de meta datos para especificar el esquema a exportar
  DBMS_DATAPUMP.METADATA_FILTER(h2,'SCHEMA_EXPR','IN (''TCHECK'')');
-- Iniciar el trabajo 
  DBMS_DATAPUMP.START_JOB(h2);
  
-- En el loop se monitorea el trabajo hasta que finaliza y
-- se muestra información del progreso
  percent_done := 0;
  job_state := 'UNDEFINED';
  while (job_state != 'COMPLETED') and (job_state != 'STOPPED') loop
    dbms_datapump.get_status(h2,
           dbms_datapump.ku$_status_job_error +
           dbms_datapump.ku$_status_job_status +
           dbms_datapump.ku$_status_wip,-1,job_state,sts);
    js := sts.job_status;
-- Si cambia el porcentaje muestra el nuevo
    if js.percent_done != percent_done
    then
      dbms_output.put_line('*** Job percent done = ' ||
                           to_char(js.percent_done));
      percent_done := js.percent_done;
    end if;
-- Si algun Trabajo en proceso (WIP) o mensaje de error se recibe 
-- en el trabajo (job) lo muestra en pantalla
   if (bitand(sts.mask,dbms_datapump.ku$_status_wip) != 0)
    then
      le := sts.wip;
    else
      if (bitand(sts.mask,dbms_datapump.ku$_status_job_error) != 0)
      then
        le := sts.error;
      else
        le := null;
      end if;
    end if;
    if le is not null
    then
      ind := le.FIRST;
      while ind is not null loop
        dbms_output.put_line(le(ind).LogText);
        ind := le.NEXT(ind);
      end loop;
    end if;
  end loop; --aqui acaba el loop principal
-- Muestra que el trabajo ha sido realizado.
  dbms_output.put_line('Job has completed');
  dbms_output.put_line('Final job state = ' || job_state);
  dbms_datapump.detach(h2);
END;