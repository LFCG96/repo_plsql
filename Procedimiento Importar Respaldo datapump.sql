--SP para importar una bd usando el paquete dbms_datapump

create or replace PROCEDURE sp_importar_respaldo
is
  ind NUMBER;              -- Loop index
  h1 NUMBER;               -- "Palanca" para realizar el trabajo
  percent_done NUMBER;     -- Percentage of job complete
  job_state VARCHAR2(30);  -- Para realizar seguimiento del estado del trabajo
  le ku$_LogEntry;         -- For WIP and error messages
  js ku$_JobStatus;        -- The job status from get_status
  jd ku$_JobDesc;          -- The job description from get_status
  sts ku$_Status;          -- The status object returned by get_status
BEGIN
-- Creamos un trabajo de Data Pump (Bomba de datos) para importar el esquema
  h1 := DBMS_DATAPUMP.OPEN('IMPORT','FULL',NULL,'IMPORTAR');
-- Especificar el nombre del archivo a importar y el directorio a utilizar
-- en el handle usado en la l�nea anterior
  DBMS_DATAPUMP.ADD_FILE(h1,'RESPALDO_TCHECK.dmp','BACKUPS');
-- Se usa un filtro de meta datos para especificar el esquema a importar, 
-- el usuario de origen y el usuario de destino
  DBMS_DATAPUMP.METADATA_REMAP(h1,'REMAP_SCHEMA','TCHECK','TCHECK');
-- Parametro para actuar en caso de que la tabla exista, puede ser 
-- replace, skip, append o truncate
  DBMS_DATAPUMP.SET_PARAMETER(h1,'TABLE_EXISTS_ACTION','REPLACE');
-- Comienza el trabajo
  DBMS_DATAPUMP.START_JOB(h1);
-- En el loop se monitorea el trabajo hasta que finaliza y
-- se muestra informaci�n del progreso
 percent_done := 0;
  job_state := 'UNDEFINED';
  while (job_state != 'COMPLETED') and (job_state != 'STOPPED') loop
    dbms_datapump.get_status(h1,
           dbms_datapump.ku$_status_job_error +
           dbms_datapump.ku$_status_job_status +
           dbms_datapump.ku$_status_wip,-1,job_state,sts);
    js := sts.job_status;
-- Si cambia el porcentaje muestra el nuevo
     if js.percent_done != percent_done
    then
      dbms_output.put_line('*** Job percent done = ' ||
                           to_char(js.percent_done));
      percent_done := js.percent_done;
    end if;
-- Si algun Trabajo en proceso (WIP) o mensaje de error se recibe 
-- en el trabajo (job) lo muestra en pantalla
       if (bitand(sts.mask,dbms_datapump.ku$_status_wip) != 0)
    then
      le := sts.wip;
    else
      if (bitand(sts.mask,dbms_datapump.ku$_status_job_error) != 0)
      then
        le := sts.error;
      else
        le := null;
      end if;
    end if;
    if le is not null
    then
      ind := le.FIRST;
      while ind is not null loop
        dbms_output.put_line(le(ind).LogText);
        ind := le.NEXT(ind);
      end loop;
    end if;
  end loop;--aqui acaba el loop principal
-- Muestra que el trabajo ha sido realizado.
  dbms_output.put_line('Job has completed');
  dbms_output.put_line('Final job state = ' || job_state);
  dbms_datapump.detach(h1);
END;