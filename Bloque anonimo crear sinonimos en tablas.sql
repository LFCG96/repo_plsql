--Crear sinonimos para un usuario a partir de las tablas de otro en un bloque anonimo :D 

BEGIN 
    FOR c IN (SELECT * FROM USER_TABLES) LOOP
        EXECUTE IMMEDIATE 'CREATE SYNONYM MI_USUARIO_2.'||c.TABLE_NAME||' FOR TCHECK.'||c.TABLE_NAME;
    END LOOP;
END;
--usa un cursor para recorrer las tablas del usuario actual y asignarle un sinonimo para cada tabla
--a otro usuario. Como es sentencia que no se puede ejecutar directamente en un bloque ya que 
--tiene parametros de tiempo real, se utiliza EXECUTE IMMEDIATE para que no sea una instrucción precompilada.
