--en consola ejecutar sqlplus y loggearse sys /as sysdba
--en consola GRANT EXECUTE ON sys.UTL_FILE TO Luis2;
SELECT * FROM DBA_TAB_PRIVS WHERE TABLE_NAME='UTL_FILE';
--desde usuario sys en sqslplus
CREATE DIRECTORY FILES AS 'C:\Users\everis\Desktop\files';

grant read, write on directory FILES to Luis2;

--SP (con Luis2)
CREATE OR REPLACE PROCEDURE sal_status(p_dir IN VARCHAR2, p_filename IN VARCHAR2) IS
    v_file UTL_FILE.FILE_TYPE;
    CURSOR empc IS
        SELECT last_name, salary, department_id
            FROM employees ORDER BY department_id;
            v_newdeptno employees.department_id%TYPE;
            v_olddeptno employees.department_id%TYPE := 0;
BEGIN
    v_file:= UTL_FILE.FOPEN (p_dir, p_filename, 'w'); -- 1
    UTL_FILE.PUT_LINE(v_file, -- 2
        'REPORT: GENERATED ON ' || SYSDATE);
    UTL_FILE.NEW_LINE (v_file); 
    FOR emp_rec IN empc LOOP
        UTL_FILE.PUT_LINE (v_file, -- 4
        ' EMPLOYEE: ' || emp_rec.last_name ||
        ' earns: ' || emp_rec.salary);
    END LOOP;
    UTL_FILE.PUT_LINE(v_file,'*** END OF REPORT ***'); -- 5
    UTL_FILE.FCLOSE (v_file); -- 6
EXCEPTION
    WHEN UTL_FILE.INVALID_FILEHANDLE THEN -- 7
        RAISE_APPLICATION_ERROR(-20001,'Invalid File.');
    WHEN UTL_FILE.WRITE_ERROR THEN -- 8
        RAISE_APPLICATION_ERROR (-20002, 'Unable to
        write to file');
END sal_status;

BEGIN sal_status('FILES','report.txt'); END;
