-- Bloques anonimo -- 
--***********************************
[DECLARE]
BEGIN 
    --STATEMENTS
[EXCEPTION]
END;


BEGIN
    DBMS_OUTPUT.PUT_LINE('PL/SQL is easy!');
END;


DECLARE
    v_date DATE := SYSDATE;
BEGIN
    DBMS_OUTPUT.PUT_LINE(v_date);
END;


DECLARE
    v_ename VARCHAR2(25);
BEGIN
    SELECT ENAME
        INTO v_ename
        FROM EMP
        WHERE ENAME='KING';
    DBMS_OUTPUT.PUT_LINE('EL EMPLEADO DEL MES ES: '|| v_ename ||'.');
EXCEPTION
    WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE('DEMASIADOS REGISTROS SOLICITADOS, USA UN CURSOR O CAMBIA EL CRITERIO.');
END;  

--SUBPROGRAMAS PROCEDIMIENTOS Y FUNCIONES--
--*************************************

--PROCEDURES--
--********************************
PROCEDURE name
IS
    --DECLARACIONES DE VARIABLES
BEGIN
    --STATEMENTS
[EXCEPTION]
END; 


CREATE OR REPLACE PROCEDURE print_date IS
    v_date VARCHAR2(30);
BEGIN
    SELECT TO_CHAR(SYSDATE,'Mon DD, YYYY')
        INTO v_date
        FROM DUAL;
    DBMS_OUTPUT.PUT_LINE(v_date);
END;
EXEC print_date;

--FUNCIONES--
--**********************************
FUNCTION name
RETURN datatype
    --DECLARACI�N DE VARIABLES
IS
BEGIN
    --STATEMENTS
    RETURN value;
    [EXCEPTION]
END


CREATE OR REPLACE FUNCTION tomorrow (P_TODAY IN DATE)
    RETURN DATE IS
    v_tomorrow DATE;
BEGIN
    SELECT p_today+1 INTO v_tomorrow FROM DUAL;
    RETURN v_tomorrow;
END;

SELECT TOMORROW(SYSDATE) AS "TOMORROW'S DATE" FROM DUAL;

--USO DE VARIABLES--
--*****************************
DECLARE
    v_counter INTEGER := 0;
BEGIN
    v_counter := v_counter + 1;
    DBMS_OUTPUT.PUT_LINE(v_counter);
END; 

DECLARE
    v_myname VARCHAR2(20);
BEGIN
    DBMS_OUTPUT.PUT_LINE('My name is: '||v_myname);
    v_myname := 'John';
    DBMS_OUTPUT.PUT_LINE('My name is: '||v_myname);
END;

--TYPO DE DATOS SCALAR--
--***********************************
DECLARE
    v_date1 DATE := '05-04-2015';
    v_date2 DATE := v_date1 + 7;
    v_date3 TIMESTAMP := SYSDATE;
    v_date4 TIMESTAMP WITH TIME ZONE := SYSDATE;
BEGIN
    DBMS_OUTPUT.PUT_LINE(v_date1);
    DBMS_OUTPUT.PUT_LINE(v_date2);
    DBMS_OUTPUT.PUT_LINE(v_date3);
    DBMS_OUTPUT.PUT_LINE(v_date4);
END;

DECLARE
    v_valid1 BOOLEAN := TRUE;
    v_valid2 BOOLEAN;
    v_valid3 BOOLEAN NOT NULL := FALSE;
BEGIN
    IF v_valid1 THEN
        DBMS_OUTPUT.PUT_LINE('Test is TRUE');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Test is FALSE');
    END IF;
END;

DECLARE
    v_first_name emp.ename%TYPE; --OBTIENE EL TIPO DE DATO DE LA COLUMNA
BEGIN
    SELECT ename
        INTO v_first_name
        FROM emp
        WHERE ename = 'KING';
    DBMS_OUTPUT.PUT_LINE(v_first_name);
END;

DECLARE
    v_desc_size INTEGER(5);
    v_prod_description VARCHAR2(70):='You can use this product with your radios for higher frequency';
BEGIN
    v_desc_size:= LENGTH(v_prod_description);
    DBMS_OUTPUT.PUT_LINE(v_desc_size);
END;

DECLARE
    v_my_num BINARY_INTEGER := -56664;
BEGIN
    DBMS_OUTPUT.PUT_LINE(SIGN(v_my_num)); --OBTIENE EL SIGNO
END;

DECLARE
    v_no_months PLS_INTEGER :=0;
BEGIN
    v_no_months := MONTHS_BETWEEN('31-01-2006','31-05-2005');
    DBMS_OUTPUT.PUT_LINE(v_no_months);
END;

--CONVERSION EXPLICITA--
--********************************
BEGIN
    DBMS_OUTPUT.PUT_LINE(TO_DATE('ABRIL-1999','Month-YYYY'));
END;

DECLARE
    v_a VARCHAR2(10) := '-123456';
    v_b VARCHAR2(10) := '+987654';
    v_c PLS_INTEGER;
BEGIN
    v_c := TO_NUMBER(v_a) + TO_NUMBER(v_b);
    DBMS_OUTPUT.PUT_LINE(v_c);
END;

--BLOQUES ANIDADOS--Y VARIABLES GLOBALES Y LOCALES
--********************************
DECLARE
    v_outer_variable VARCHAR2(20):='GLOBAL VARIABLE';
BEGIN
    DECLARE
        v_inner_variable VARCHAR2(20):='LOCAL VARIABLE';
    BEGIN
        DBMS_OUTPUT.PUT_LINE(v_inner_variable);
        DBMS_OUTPUT.PUT_LINE(v_outer_variable);
    END;
    DBMS_OUTPUT.PUT_LINE(v_outer_variable);
END;

<<outer>>
DECLARE
    v_father_name VARCHAR2(20):='Patrick';
    v_date_of_birth DATE:='20-04-1972';
BEGIN
    DECLARE
        v_child_name VARCHAR2(20):='Mike';
        v_date_of_birth DATE:='12-12-2002';
    BEGIN
        DBMS_OUTPUT.PUT_LINE('Father''s Name: ' || v_father_name);
        DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || outer.v_date_of_birth);
        DBMS_OUTPUT.PUT_LINE('Child''s Name: ' || v_child_name);
        DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || v_date_of_birth);
    END;
END;

--SELECT CON M�S DE UN CAMPO ASIGNADO A VARIABLES--
--******************************************************
DECLARE 
    v_emp_hiredate emp.hiredate%TYPE; 
    v_emp_salary emp.sal%TYPE; 
BEGIN 
    SELECT  hiredate, sal 
        INTO   v_emp_hiredate, v_emp_salary 
        FROM   emp
        WHERE  ename = 'KING';   
    DBMS_OUTPUT.PUT_LINE('Hiredate: ' || v_emp_hiredate); 
    DBMS_OUTPUT.PUT_LINE('Salary: '|| v_emp_salary); 
END;
--SELECT CON FUNCION DE GRUPO--
--***********************************++
DECLARE  
    v_sum_sal NUMBER(10,2); 
    v_deptno NUMBER NOT NULL := 30;      
BEGIN 
    SELECT SUM(sal) -- group function 
        INTO v_sum_sal 
        FROM emp 
        WHERE deptno = v_deptno; 
    DBMS_OUTPUT.PUT_LINE('Dep #30 Salary Total: ' || v_sum_sal); 
END;

--MANIPULANDO DATOS EN PLSQL--
--**********************************
SELECT* FROM EMP;

BEGIN 
    INSERT INTO EMP VALUES
        (1111,'LUIS','CEO',7839,SYSDATE,10000,NULL,10);
END;

DECLARE 
    v_sal_increase EMP.SAL%TYPE := 800;
BEGIN
    UPDATE EMP
        SET SAL = SAL + v_sal_increase
        WHERE EMPNO=1111;
END;

DECLARE 
    v_empno EMP.EMPNO%TYPE := 1111;
BEGIN
    DELETE FROM EMP
        WHERE EMPNO=v_empno;
END;

--CURSORES IMPLICITOS--
--*************************************
BEGIN 
    INSERT INTO EMP VALUES
        (1111,'LUIS','CEO',7839,SYSDATE,10000,NULL,10);
    DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' LINEAS INSERTADAS.');
END;

DECLARE 
    v_sal_increase EMP.SAL%TYPE := 800;
BEGIN
    UPDATE EMP
        SET SAL = SAL + v_sal_increase
        WHERE EMPNO=1111;
    DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' LINEAS ACTUALIZADAS.');
END;

DECLARE 
    v_empno EMP.EMPNO%TYPE := 1111;
BEGIN
    DELETE FROM EMP
        WHERE EMPNO=v_empno;
    DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' LINEAS ELIMINADAS.');
END;

--CONTROL DE TRANSACCIONES--
--*****************************************
BEGIN 
    INSERT INTO A VALUES (9); 
    ROLLBACK; 
    INSERT INTO A VALUES (10); 
    COMMIT; 
END;
SELECT*FROM A; --SOLO INSERTA 10

BEGIN 
    INSERT INTO B VALUES (11); 
    SAVEPOINT my_sp_1; 
    INSERT INTO B VALUES (12); 
    SAVEPOINT my_sp_2; 
    INSERT INTO B VALUES (13); 
    ROLLBACK to my_sp_1; 
    INSERT INTO B VALUES (14); 
    COMMIT; 
END;
SELECT * FROM B;--SOLO INSERTA 11 Y 14

--SENTENCIAS DE CONTROL-- IF
--****************************************+
DECLARE 
    v_myage NUMBER := 31; 
BEGIN 
    IF v_myage < 11 THEN 
        DBMS_OUTPUT.PUT_LINE('I am a child'); 
    ELSIF v_myage < 20 THEN 
        DBMS_OUTPUT.PUT_LINE('I am young'); 
    ELSIF v_myage < 30 THEN 
        DBMS_OUTPUT.PUT_LINE('I am in my twenties'); 
    ELSIF v_myage < 40 THEN 
        DBMS_OUTPUT.PUT_LINE('I am in my thirties'); 
    ELSE 
        DBMS_OUTPUT.PUT_LINE('I am mature'); 
    END IF; 
END;

DECLARE 
    v_myage NUMBER := 1; 
    v_myfirstname VARCHAR2(11) := 'Christopher'; 
BEGIN 
    IF v_myfirstname ='Christopher' AND v_myage < 11 THEN 
        DBMS_OUTPUT.PUT_LINE('I am a child named Christopher');  
    END IF; 
END;

--SENTENCIA CASE--
--****************************************************
DECLARE 
    v_num NUMBER := 15; 
    v_txt VARCHAR2(50); 
BEGIN 
    CASE v_num 
        WHEN 20 THEN v_txt := 'number equals 20'; 
        WHEN 17 THEN v_txt := 'number equals 17'; 
        WHEN 15 THEN v_txt := 'number equals 15'; 
        WHEN 13 THEN v_txt := 'number equals 13'; 
        WHEN 10 THEN v_txt := 'number equals 10'; 
        ELSE v_txt := 'some other number'; 
    END CASE; 
    DBMS_OUTPUT.PUT_LINE(v_txt); 
END;

--SEARCHED CASE--
DECLARE 
    v_num NUMBER := 15; 
    v_txt VARCHAR2(50); 
BEGIN 
    CASE 
        WHEN v_num > 20 THEN v_txt := 'greater than 20'; 
        WHEN v_num > 15 THEN v_txt := 'greater than 15'; 
        ELSE v_txt := 'less than 16'; 
    END CASE; 
    DBMS_OUTPUT.PUT_LINE(v_txt);
END;
--ASIGNAR UN VALOR CON CASE--
DECLARE 
    v_in_var NUMBER := 50; 
    v_out_var VARCHAR2(15); 
BEGIN 
    v_out_var := CASE v_in_var
        WHEN 1 THEN 'LOW VALUE'
        WHEN 50 THEN 'MIDDLE VALUE'
        ELSE 'HIGH VALUE'
    END; 
    DBMS_OUTPUT.PUT_LINE(v_out_var);
END;

DECLARE 
    v_grade CHAR(1) := 'A'; 
    v_appraisal VARCHAR2(20); 
BEGIN 
    v_appraisal := CASE -- no selector here 
        WHEN v_grade = 'A' THEN 'Excellent' 
        WHEN v_grade IN ('B','C') THEN 'Good'          
        ELSE 'No such grade'  
    END; 
    DBMS_OUTPUT.PUT_LINE ('Grade: '|| v_grade || ' Appraisal ' || v_appraisal); 
END;

--BASIC LOOPS--
--*************************************+++
DECLARE 
    v_counter NUMBER(2) := 1; 
BEGIN 
    LOOP 
        DBMS_OUTPUT.PUT_LINE('Loop execution #' || v_counter); 
        v_counter := v_counter + 1; 
        EXIT WHEN v_counter > 5; 
    END LOOP; 
END;

DECLARE 
    v_counter NUMBER := 1; 
BEGIN 
    LOOP 
        DBMS_OUTPUT.PUT_LINE('Counter is ' || v_counter); 
        v_counter := v_counter + 1; 
        IF v_counter > 10 THEN 
            EXIT;
        END IF; 
    END LOOP; 
END;

--WHILE LOOPS--
--****************************************
DECLARE
    v_temp_a NUMBER := 20;
BEGIN
    WHILE v_temp_a <=23 LOOP
        INSERT INTO A VALUES(v_temp_a);
        v_temp_a := v_temp_a + 1;
    END LOOP;
END;
SELECT * FROM A;

--FOR LOOPS--
--**********************
BEGIN
    FOR i IN 30..32 LOOP
        INSERT INTO A VALUES (i);
    END LOOP;
END;
--LOOPS ANIDADOS (ESTRUCTURA)--
--************************
DECLARE 
    ...
BEGIN 
    <<outer_loop>> 
    LOOP             -- outer loop ... 
        <<inner_loop>> 
        LOOP           -- inner loop 
            EXIT outer_loop WHEN v_outer_done  -- exits both loops 
            EXIT WHEN v_inner_done = 'YES';   ...             
        END LOOP; 
        ...
        EXIT WHEN v_outer_done = 'YES'; ... 
    END LOOP; 
END;

--CURSORES EXPLICITOS
--*****************************
DECLARE
    CURSOR cur_dept IS
        SELECT DEPTNO, DNAME FROM DEPT;
    v_deptno DEPT.DEPTNO%TYPE;
    v_dname DEPT.DNAME%TYPE;
BEGIN
    OPEN cur_dept;
    LOOP
        FETCH cur_dept INTO v_deptno,v_dname;
        EXIT WHEN cur_dept%notfound;
        DBMS_OUTPUT.PUT_LINE(v_deptno||' '||v_dname);
    END LOOP;
    CLOSE cur_dept;
END;

DECLARE --en este ejemplo usaremos un dato record para guardar filas completas
    CURSOR cur_dept IS
        SELECT * FROM DEPT;
    v_dept_record cur_dept%ROWTYPE;
BEGIN
    OPEN cur_dept;
    LOOP
        FETCH cur_dept INTO v_dept_record;
        EXIT WHEN cur_dept%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_dept_record.DEPTNO||' '||v_dept_record.DNAME||' '||v_dept_record.LOC);
    END LOOP;
    CLOSE cur_dept;
END;

--ATRIBUTOS 
DECLARE 
    CURSOR cur_dept IS
        SELECT * FROM DEPT;
    v_dept_record cur_dept%ROWTYPE;
BEGIN
    IF NOT cur_dept%ISOPEN THEN --ATRIBUTO %ISOPEN
        OPEN cur_dept;
    END IF;
    LOOP
        FETCH cur_dept INTO v_dept_record;
        EXIT WHEN cur_dept%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_dept_record.DEPTNO||' '||v_dept_record.DNAME||' '||v_dept_record.LOC);
    END LOOP;
    CLOSE cur_dept;
END;

--CURSORES CON FOR LOOPS-- ABRE HACE FETCH Y CIERRA AUTOMATICAMENTE
--***********************************************
DECLARE 
    CURSOR cur_dept IS
        SELECT DEPTNO,DNAME,LOC FROM DEPT;
BEGIN
    FOR v_dept_record IN cur_dept LOOP
        DBMS_OUTPUT.PUT_LINE(v_dept_record.DEPTNO||' ' ||v_dept_record.DNAME ||' '||v_dept_record.LOC);
    END LOOP;
END;

BEGIN --CON SUBQUERYS
    FOR v_dept_record IN (SELECT DEPTNO,DNAME,LOC FROM DEPT) LOOP
        DBMS_OUTPUT.PUT_LINE(v_dept_record.DEPTNO||' ' ||v_dept_record.DNAME ||' '||v_dept_record.LOC);
    END LOOP;
END;

--CURSORES CON PARAMETROS--SE PUEDE ABRIR VARIAS VECES CON UN NUEVO PARAMETRO :D
--************************************************
DECLARE
    CURSOR cur_dept (p_loc VARCHAR2) IS
        SELECT DEPTNO, DNAME, LOC FROM DEPT
            WHERE LOC = p_loc;                                   --PARAMETROS :D
    v_dept_record cur_dept%ROWTYPE;
BEGIN
    OPEN cur_dept ('BOSTON');  --AQUI SE MANDA EL PARAMETRO :D
    LOOP
        FETCH cur_dept INTO v_dept_record;
        EXIT WHEN cur_dept%notfound;
        DBMS_OUTPUT.PUT_LINE(v_dept_record.DEPTNO||' '||v_dept_record.DNAME||' '||v_dept_record.LOC);
    END LOOP;
    CLOSE cur_dept;
    OPEN cur_dept ('CHICAGO');  --AQUI SE MANDA EL PARAMETRO :D
    LOOP
        FETCH cur_dept INTO v_dept_record;
        EXIT WHEN cur_dept%notfound;
        DBMS_OUTPUT.PUT_LINE(v_dept_record.DEPTNO||' '||v_dept_record.DNAME||' '||v_dept_record.LOC);
    END LOOP;
    CLOSE cur_dept;
END;

--RECORDS DEFINIDOS POR EL USUARIO--
--*****************************************
DECLARE
    v_emp_rec EMP%ROWTYPE;
BEGIN
    SELECT * INTO v_emp_rec
        FROM EMP 
        WHERE ENAME='KING';
    DBMS_OUTPUT.PUT_LINE('EMPNO: '||v_emp_rec.EMPNO||' Nombre: '||v_emp_rec.ENAME);
END;

--DECLARAR UN TYPO Y ASIGNARLO A UN RECORD (JOINS)
DECLARE
    TYPE person_emp IS RECORD
    (EMPNO EMP.EMPNO%TYPE,
    ENAME EMP.ENAME%TYPE,
    DNAME DEPT.DNAME%TYPE);
    v_emp_rec person_emp;
BEGIN
    SELECT e.EMPNO, e.ENAME, d.DNAME INTO v_emp_rec
        FROM EMP e JOIN DEPT d
        ON e.DEPTNO = D.DEPTNO
        WHERE ENAME='KING';
    DBMS_OUTPUT.PUT_LINE('EMPNO: '||v_emp_rec.EMPNO||' Nombre: '||v_emp_rec.ENAME||' Departamento: '||v_emp_rec.DNAME);
END;

--COLLECTIONS--INDEX BY TABLE--
--***********************************
DECLARE --para un tipo de dato
    TYPE t_hire_date IS TABLE OF emp.hiredate%TYPE
    INDEX BY BINARY_INTEGER;
    v_hire_date_tab t_hire_date;
BEGIN
    FOR emp_rec IN (SELECT empno, hiredate FROM emp)
    LOOP
        v_hire_date_tab(emp_rec.empno):= emp_rec.hiredate;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE(v_hire_date_tab.COUNT); --esta es una de las funciones del collection
END;

DECLARE --usando records
    TYPE t_emp_rec IS TABLE OF emp%ROWTYPE
        INDEX BY BINARY_INTEGER;
    v_emp_rec_tab t_emp_rec;
BEGIN
    FOR emp_rec IN (SELECT * FROM emp) LOOP
        v_emp_rec_tab(emp_rec.empno) := emp_rec;
        DBMS_OUTPUT.PUT_LINE(v_emp_rec_tab(emp_rec.empno).sal);
    END LOOP;
END;

--MANEJO DE EXCEPCIONES--
--****************************************++
DECLARE
    v_name emp.ename%TYPE := 'KINGs';
    v_job emp.job%TYPE;
BEGIN
    SELECT job INTO v_job
        FROM emp WHERE ename = v_name ;
    DBMS_OUTPUT.PUT_LINE ('Puesto, ' || v_job );
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE ('Puesto, ' || v_name || ',
            cannot be found. Re-enter the employee name using the correct
            spelling.');
        WHEN TOO_MANY_ROWS THEN
            DBMS_OUTPUT.PUT_LINE ('DEMASIADOS ARGUMENTOS, USA UN CURSOR.');
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE ('OCURRIO UN ERROR.');
END;

--excepci�n creada por mi
DECLARE
    e_invalid_department EXCEPTION;
    v_name VARCHAR2(20):='Accounting';
    v_deptno NUMBER := 27;
BEGIN
    UPDATE dept
        SET dname = v_name
        WHERE deptno = v_deptno;
    IF SQL%NOTFOUND THEN
        RAISE e_invalid_department;
    END IF;
EXCEPTION
    WHEN e_invalid_department THEN 
        DBMS_OUTPUT.PUT_LINE('No such department id.');
END;

--o bien, usamos raise_application_error(-numero 20000 para arriba, mensaje)
DECLARE
    v_name VARCHAR2(20):='Accounting';
    v_deptno NUMBER := 27;
BEGIN
    UPDATE dept
        SET dname = v_name
        WHERE deptno = v_deptno;
    IF SQL%NOTFOUND THEN
        RAISE_APPLICATION_ERROR(-20202,'NO EXISTE EL DEPARTAMENTO');
    END IF;
END;

DECLARE
    v_deptno NUMBER := 27;
    v_dept DEPT%ROWTYPE;
BEGIN
    SELECT * INTO v_dept FROM DEPT WHERE DEPTNO=v_deptno;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20200,'NO EXISTE EL DEPTNO');
END;

--PROCEDIMIENTOS ALMACENADOS--
--*************************************+++
CREATE OR REPLACE PROCEDURE add_dept IS
    v_deptno dept.deptno%TYPE;
    v_dname dept.dname%TYPE;
    v_loc dept.loc%TYPE;
BEGIN
    v_deptno := 70;
    v_dname := 'Confianza';
    v_loc := 'Mexico';
    INSERT INTO dept values (v_deptno,v_dname,v_loc);
    DBMS_OUTPUT.PUT_LINE('INSERTED '|| SQL%ROWCOUNT||' ROW.');
END;

EXEC add_dept;
SELECT * FROM DEPT;

--uno con parametros
CREATE OR REPLACE PROCEDURE up_dept (v_deptno IN DEPT.deptno%TYPE, v_loc IN DEPT.loc%TYPE) IS
BEGIN
    UPDATE DEPT SET LOC = v_loc WHERE DEPTNO = v_deptno;
    DBMS_OUTPUT.PUT_LINE('UPDATED '|| SQL%ROWCOUNT||' ROW.');
END;

EXEC up_dept (70,'MEXICOS');

CREATE OR REPLACE PROCEDURE cons_dept (v_deptno IN DEPT.deptno%TYPE, v_dname OUT DEPT.dname%TYPE, v_loc OUT DEPT.loc%TYPE) IS
BEGIN
    SELECT dname, loc into v_dname, v_loc FROM DEPT WHERE DEPTNO = v_deptno;
END;

DECLARE --USANDO PARAMETROS OUT
    v_dname dept.dname%TYPE;
    v_loc dept.loc%TYPE;
BEGIN
    cons_dept(40, v_dname,v_loc);
    DBMS_OUTPUT.PUT_LINE(v_dname||v_loc);
END;

--FUNCIONES--
--****************************
CREATE OR REPLACE FUNCTION get_sal (p_id IN emp.empno%TYPE)
    RETURN NUMBER IS
        v_sal emp.sal%TYPE := 0;
BEGIN
    SELECT sal
        INTO v_sal
        FROM emp
        WHERE empno = p_id;
    RETURN v_sal;
END get_sal;

SELECT get_sal(7839) from dual;
SELECT ename, get_sal(empno) from emp;

SELECT OBJECT_NAME FROM USER_OBJECTS WHERE OBJECT_TYPE = 'FUNCTION';
--VEMOS LAS FUNCIONES DE ESTE USUARIO

--TRANSACCIONES AUTONOMAS--PARA PODER HACER M�S DE UNA TRANSACCI�N A LA VEZ
--***************************************************************************

--AUTONOMA
PROCEDURE at_proc IS
    PRAGMA
        AUTONOMOUS_TRANSACTION; --ASI SE PUEDE EJECUTAR MIENTRAS OTRA TRANSACCI�N PRINCIPAL SE EJECUTA
    dept_id NUMBER := 90;
BEGIN
    UPDATE ...
    INSERT ...
    COMMIT; -- Required
END at_proc;

--MAIN
PROCEDURE mt_proc IS
    emp_id NUMBER;
BEGIN
    emp_id := 1234;
    INSERT ...
    at_proc; --AQU� SE EJECUTAR�A MIENTRAS LA TRANSACCI�N PRINCIPAL SE TERMINA DE EJECUTAR
    DELETE ...
    COMMIT; -- or ROLLBACK;
END mt_proc;

--****************************************
--CREACI�N DE PAQUETES--
--****************************************
CREATE OR REPLACE PACKAGE check_emp_pkg IS
    g_max_length_of_service CONSTANT NUMBER := 100;
    PROCEDURE chk_hiredate (p_date IN emp.hiredate%TYPE);
    PROCEDURE chk_dept_mgr (p_empid IN emp.empno%TYPE, p_mgr IN emp.mgr%TYPE);
END check_emp_pkg;

CREATE OR REPLACE PACKAGE BODY check_emp_pkg IS
    PROCEDURE chk_hiredate (p_date IN emp.hiredate%TYPE)IS 
    BEGIN
        IF MONTHS_BETWEEN(SYSDATE, p_date) > g_max_length_of_service * 12 THEN
            RAISE_APPLICATION_ERROR(-20200, 'Invalid Hiredate');
        END IF;
    END chk_hiredate;
    
    PROCEDURE chk_dept_mgr (p_empid IN emp.empno%TYPE, p_mgr IN emp.mgr%TYPE) IS 
    BEGIN 
        dbms_output.put_line('luego definimos esto xD');
    END chk_dept_mgr;
END check_emp_pkg;

--para iinvocar un subprograma de un paquete es necesario indicar el nombre del paquete 
-- por ejemplo check_emp_pkg.chk_hiredate('01/01/2020');

DROP PACKAGE check_emp_pkg; --borra la especificaci�n y el body
DROP PACKAGE BODY check_emp_pkg;--borra solo el body

--para ver errores sobre procedimientos
SELECT line, text, position -- where and error message
    FROM USER_ERRORS
    WHERE name = 'BAD_PROC' AND type = 'PROCEDURE'
    ORDER BY sequence; 

SELECT e.line, e.position, s.text AS SOURCE, e.text AS ERROR
    FROM USER_ERRORS e, USER_SOURCE s
    WHERE e.name = s.name AND e.type = s.type
    AND e.line = s.line
    AND e.name = 'BAD_PROC' and e.type = 'PROCEDURE'
    ORDER BY e.sequence;
    
--SOBRECARGA EN PAQUETES--
--****************************+
CREATE OR REPLACE PACKAGE emp_pkg IS
    PROCEDURE find_emp -- 1
        (p_employee_id IN NUMBER, p_last_name OUT VARCHAR2);
    PROCEDURE find_emp -- 2
        (p_job_id IN VARCHAR2, p_last_name OUT VARCHAR2);
    PROCEDURE find_emp -- 3
        (p_hiredate IN DATE, p_last_name OUT VARCHAR2);
END emp_pkg;  --SOBRECARGA PARA ENCONTRAR EMPS POR DIFERENTES PARAMETROS

DECLARE v_last_name VARCHAR2(30);
    BEGIN emp_pkg.find_emp('IT_PROG', v_last_name);
END; --USA LA SOBRECARGA #2

--BODILESS PACKAGES--
CREATE OR REPLACE PACKAGE global_consts IS
    mile_to_kilo CONSTANT NUMBER := 1.6093;
    kilo_to_mile CONSTANT NUMBER := 0.6214;
    yard_to_meter CONSTANT NUMBER := 0.9144;
    meter_to_yard CONSTANT NUMBER := 1.0936;
END global_consts;

DECLARE--AQUI USAMOS ESAS CONSTANTES :D
    distance_in_miles NUMBER(5) := 5000;
    distance_in_kilo NUMBER(6,2);
BEGIN
    distance_in_kilo :=
    distance_in_miles * global_consts.mile_to_kilo;
    DBMS_OUTPUT.PUT_LINE(distance_in_kilo);
END;

--Lectura de archivos (solo ejemplos no funcionales porque no tengo los archivos XD)
--*****************************************************
PROCEDURE read(dir VARCHAR2, filename VARCHAR2) IS
    file UTL_FILE.FILE_TYPE;
BEGIN
    IF NOT UTL_FILE.IS_OPEN(file) THEN
        file := UTL_FILE.FOPEN (dir, filename, 'r');
    END IF; 
    ...
END read;

--ejemplo de UTL_MAIL--
--************************
BEGIN
    UTL_MAIL.SEND('database@oracle.com',
        'joe43@yahoo.com',
        message => 'Friday�s meeting will be at 10:30 in
        Room 6',
        subject => 'Our PL/SQL meeting');
END;
--EL ORDEN ES:
--SENDER,DESTINO,MENSAJE,ASUNTO.



--*******************************************
--              SQL DIN�MICO               --
--*******************************************
CREATE PROCEDURE drop_any_table(p_table_name VARCHAR2) IS
BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE ' || p_table_name;
END; --este procedimiento borra cualquier tabla

--ahora lo creamos en una variable
CREATE PROCEDURE drop_any_table2(p_table_name VARCHAR2) IS
    v_dynamic_stmt VARCHAR2(50);
BEGIN
    v_dynamic_stmt := 'DROP TABLE ' || p_table_name;
    EXECUTE IMMEDIATE v_dynamic_stmt;
END;

--borra los rows y retorna cuantos fueron
CREATE FUNCTION del_rows(p_table_name VARCHAR2) RETURN NUMBER IS
BEGIN
    EXECUTE IMMEDIATE 'DELETE FROM ' || p_table_name;
    RETURN SQL%ROWCOUNT;
END;

--ejecutando
DECLARE
    v_count NUMBER;
BEGIN
    v_count := del_rows('EMPLOYEE_NAMES');
    DBMS_OUTPUT.PUT_LINE(v_count || ' rows deleted.');
END;

--PARA RECOMPILAR PLQSL OBJECTS
ALTER PROCEDURE procedure-name COMPILE;
ALTER FUNCTION function-name COMPILE;
ALTER PACKAGE package_name COMPILE SPECIFICATION;
ALTER PACKAGE package-name COMPILE BODY;

--EN DYNAMIC SQL SER�A
CREATE PROCEDURE compile_plsql
    (p_name VARCHAR2,p_type VARCHAR2,p_options VARCHAR2 := NULL) IS
        v_stmt VARCHAR2(200);
BEGIN
    v_stmt := 'ALTER ' || p_type || ' ' || p_name || ' COMPILE'
    || ' ' || p_options;
    EXECUTE IMMEDIATE v_stmt;
END;
--Y SE EJECUTA:
BEGIN compile_plsql('MYPACK','PACKAGE','BODY'); END;


--AHORA CON DBMS_SQL.--
--*********************+
--BORRA ROWS
CREATE OR REPLACE FUNCTION del_rows
    (p_table_name VARCHAR2) RETURN NUMBER IS
        v_csr_id INTEGER;
        v_rows_del NUMBER;
BEGIN
    v_csr_id := DBMS_SQL.OPEN_CURSOR;
    DBMS_SQL.PARSE(v_csr_id,
    'DELETE FROM ' || p_table_name, DBMS_SQL.NATIVE);
    v_rows_del := DBMS_SQL.EXECUTE(v_csr_id);
    DBMS_SQL.CLOSE_CURSOR(v_csr_id);
    RETURN v_rows_del;
END;

--MEJORANDO RENDIMIENTO DE PL/SQL--
--*************************************
CREATE OR REPLACE PACKAGE emp_pkg IS
    TYPE t_emp IS TABLE OF employees%ROWTYPE
        INDEX BY BINARY_INTEGER;
    PROCEDURE emp_proc (p_small_arg IN NUMBER, p_big_arg OUT NOCOPY t_emp); --AL DECLARAR OUT Y NOCOY SOLO SE MANEJAR� UNA SIMPLE COPIA DE DATOS Y NO DOS
END emp_pkg;

--TABLAS BASADAS EN INDEX CON CALCULOS DETERMINISTICOS
CREATE OR REPLACE FUNCTION twicenum
    (p_number IN NUMBER)
    RETURN NUMBER DETERMINISTIC IS
BEGIN
    RETURN p_number * 2;
END twicenum;

CREATE INDEX emp_twicesal_idx
ON emp(twicenum(sal)); 

--usando BULK BINDING--
--**************************
CREATE OR REPLACE PROCEDURE fetch_all_emps IS
    TYPE t_emp IS TABLE OF employees%ROWTYPE INDEX BY BINARY_INTEGER;
    v_emptab t_emp;
BEGIN
    SELECT * BULK COLLECT INTO v_emptab FROM employees; --CON UN TABLE INDEX PODEMOS ASIGNAR TODOS LOS REGISTROS EN UN SOLO "JALON"
    FOR i IN v_emptab.FIRST..v_emptab.LAST LOOP
        IF v_emptab.EXISTS(i) THEN
            DBMS_OUTPUT.PUT_LINE(v_emptab(i).last_name);
        END IF;
    END LOOP;
END fetch_all_emps;

--RETURNING CLAUSE -- PARA NO TENER QUE HACER TANTAS CONSULTAS--
CREATE OR REPLACE PROCEDURE update_one_emp
    (p_emp_id IN emp.empNO%TYPE,
    p_salary_raise_percent IN NUMBER) IS
        v_new_salary emp.sal%TYPE;
BEGIN
    UPDATE emp
        SET sal = sal * (1 + p_salary_raise_percent)
        WHERE empno = p_emp_id
        RETURNING sal INTO v_new_salary; --RETORNA EL VALOR ACTUALIZADO DEL REGSTRO EN UNA VARIABLE
    DBMS_OUTPUT.PUT_LINE('New salary is: ' || v_new_salary);
END update_one_emp;


--*********************************
--           TRIGGERS            --
--*********************************
CREATE OR REPLACE TRIGGER log_sal_change_trigg
AFTER UPDATE OF sal ON emp
BEGIN
    INSERT INTO log_table (user_id, logon_date)
        VALUES (USER, SYSDATE);
END; --AL ACTUALIZAR SE LLENA UN REGISTRO DE LOG_TABLE


--CREAR UN REGISTRO DE LOGGING (NECESITAMOS CREAR LOG_TABLE)
CREATE TABLE log_table (
user_id VARCHAR2(30),
logon_date DATE);

CREATE OR REPLACE TRIGGER logon_trigg
    AFTER LOGON ON DATABASE
BEGIN
    INSERT INTO log_table (user_id, logon_date)
        VALUES (USER, SYSDATE);
END;

SELECT * FROM LOG_TABLE;

--NO PERMITE CAMBIAR JOBID SI ALGUNA VEZ YA LO TUVO
CREATE OR REPLACE TRIGGER check_sal_trigg
    BEFORE UPDATE OF job_id ON employees
    FOR EACH ROW
DECLARE
    v_job_count INTEGER;
BEGIN
    SELECT COUNT(*) INTO v_job_count
        FROM job_history
        WHERE employee_id = :OLD.employee_id --OLD PORQUE NO SE MODIFICA
        AND job_id = :NEW.job_id; --NEW PORQUE ES EL QUE SE QUIERE CAMBIAR
    IF v_job_count > 0 THEN
        RAISE_APPLICATION_ERROR
        (-20201,'This employee has already done this job');
    END IF;
END;

--DML TRIGGERS (INSERT-UPDATE-DELETE)--
--***************************************
CREATE OR REPLACE TRIGGER secure_emp --EVITA INSERCIONES EN FINES DE SEMANA
    BEFORE INSERT ON employees
BEGIN
    IF TO_CHAR(SYSDATE, 'DY') IN ('SAT', 'SUN') THEN
        RAISE_APPLICATION_ERROR(-20500,
        'You may insert into EMPLOYEES table only during
        business hours');
    END IF;
END;

--USANDO PREDICADOS CONDICIONALES--
CREATE OR REPLACE TRIGGER secure_emp
    BEFORE INSERT OR UPDATE OR DELETE ON employees
BEGIN
    IF TO_CHAR(SYSDATE, 'DY') IN ('SAT', 'SUN') THEN
        IF DELETING THEN RAISE_APPLICATION_ERROR
            (-20501,'You may delete from EMPLOYEES table only during business
            hours');
        ELSIF INSERTING THEN RAISE_APPLICATION_ERROR
            (-20502,'You may insert into EMPLOYEES table only during business
            hours');
        ELSIF UPDATING THEN RAISE_APPLICATION_ERROR
            (-20503,'You may update EMPLOYEES table only during business hours');
        END IF;
    END IF;
END;

--IDENTIFICA QUE COLUMNA MODIFICA--
CREATE OR REPLACE TRIGGER secure_emp
    BEFORE UPDATE ON employees
BEGIN
    IF UPDATING('SALARY') THEN
        IF TO_CHAR(SYSDATE, 'DY') IN ('SAT', 'SUN')
            THEN RAISE_APPLICATION_ERROR
            (-20501,'You may not update SALARY on the weekend');
        END IF;
    ELSIF UPDATING('JOB_ID') THEN
        IF TO_CHAR(SYSDATE, 'DY') = 'SUN'
            THEN RAISE_APPLICATION_ERROR
            (-20502, 'You may not update JOB_ID on Sunday');
        END IF;
    END IF;
END;

--ROW TRIGGER--

CREATE OR REPLACE TRIGGER log_emps
    AFTER UPDATE OF salary ON employees FOR EACH ROW --TIENE FOR EACH ROW
BEGIN
    INSERT INTO log_emp_table (who, when)
        VALUES (USER, SYSDATE);
END;

--USING :OLD Y :NEW
--**********************
CREATE OR REPLACE TRIGGER log_emps
    AFTER UPDATE OF salary ON employees FOR EACH ROW
BEGIN
    INSERT INTO log_emp_table
        (who, when, which_employee, old_salary, new_salary)
        VALUES (USER, SYSDATE, :OLD.employee_id,
        :OLD.salary, :NEW.salary);
END;

--REFERENCIANDO OLD Y NEW A OTROS NOMBRES 
CREATE OR REPLACE TRIGGER log_emps
    AFTER UPDATE OF salary ON old
    REFERENCING OLD as former NEW as latter FOR EACH ROW
BEGIN
    INSERT INTO log_emp_table (who, when, which_employee,
        old_salary, new_salary)
        VALUES (USER, SYSDATE, :former.employee_id,
        :former.salary, :latter.salary);
END;

--CLAUSULA WHEN-- SOLO SE EJECUTA SI SE CUMPLE
CREATE OR REPLACE TRIGGER restrict_salary
    AFTER UPDATE of salary ON copy_employees FOR EACH ROW
    WHEN(NEW.salary > OLD.salary)
BEGIN
    INSERT INTO log_emp_table
        (who,when,which_employee,old_salary,new_salary)
        VALUES(USER,SYSDATE,:OLD.employee_id,:OLD.salary,
        :NEW.salary);
END;
 
--TRIGGERS INSTEAD OF--
--********************************
--por ejemplo en vistas con joins en vez de insertar directamente
--en la vista, lo que hace es insertar en las tablas que la componen
CREATE OR REPLACE TRIGGER new_emp_dept
    INSTEAD OF INSERT ON emp_details --emp_details es una vista de joins
BEGIN
    INSERT INTO new_emps
        VALUES (:NEW.employee_id, :NEW.last_name,
        :NEW.salary, :NEW.department_id);
    UPDATE new_depts
        SET dept_sal = dept_sal + :NEW.salary
        WHERE department_id = :NEW.department_id;
END;

--COMPOUND TRIGGER--
--********************
CREATE OR REPLACE TRIGGER log_emps
    FOR UPDATE OF salary ON employees
    COMPOUND TRIGGER
    TYPE t_log_emp IS TABLE OF log_table%ROWTYPE
        INDEX BY BINARY_INTEGER;
    log_emp_tab t_log_emp;
AFTER EACH ROW IS --SE EJECUTA DESPUES DE CADA ROW
    BEGIN
    ... Populate log_emp_tab with employees� change data
    END AFTER EACH ROW;
AFTER STATEMENT IS --SE EJECUTA DESPUES DE TODA UNA CONSULTA
    BEGIN
    FORALL ...
    END AFTER STATEMENT;
END log_emps;

CREATE OR REPLACE TRIGGER log_emps
    FOR UPDATE OF salary ON copy_employees COMPOUND TRIGGER
    TYPE t_log_emp IS TABLE OF log_table%ROWTYPE INDEX BY
        BINARY_INTEGER;
    log_emp_tab t_log_emp;
    v_index BINARY_INTEGER := 0;
AFTER EACH ROW IS
    BEGIN
    v_index := v_index + 1;
    log_emp_tab(v_index).employee_id := :OLD.employee_id;
    log_emp_tab(v_index).change_date := SYSDATE;
    log_emp_tab(v_index).salary := :NEW.salary;
END AFTER EACH ROW;
AFTER STATEMENT IS
    BEGIN
    FORALL I IN log_emp_tab.FIRST..log_emp_tab.LAST
    INSERT INTO log_table VALUES log_emp_tab(i);
END AFTER STATEMENT;
END log_emps;

--DDL Y DATABASE TIGGERS--
--*****************************
CREATE OR REPLACE TRIGGER log_create_trigg
    AFTER CREATE ON SCHEMA
BEGIN
    INSERT INTO log_table
        VALUES (USER, SYSDATE);
END; --VA A EJECUTARSE DESPUES DE QUE UN OBJETO SE CREE EN MI SCHEMA

CREATE OR REPLACE TRIGGER prevent_drop_trigg
    BEFORE DROP ON SCHEMA
BEGIN
    RAISE_APPLICATION_ERROR
        (-20203, 'Attempted drop � failed');
END; --EVITA QUE SE BORREN OBJETOS DE MI SCHEMA

CREATE OR REPLACE TRIGGER logon_trig
    AFTER LOGON ON SCHEMA
BEGIN
    INSERT INTO log_trig_table(user_id,log_date,action)
        VALUES (USER, SYSDATE, 'Logging on');
END; --CADA QUE SE LOGGEAN EN MI SCHEMA

CREATE OR REPLACE TRIGGER logoff_trig
    BEFORE LOGOFF ON SCHEMA
BEGIN
    INSERT INTO log_trig_table(user_id,log_date,action)
        VALUES (USER, SYSDATE, 'Logging off');
END;--CADA QUE ALGUIEN SE SALE DE MI SCHEMA

CREATE OR REPLACE TRIGGER servererror_trig
    AFTER SERVERERROR ON SCHEMA
BEGIN
    IF (IS_SERVERERROR (942)) THEN
        INSERT INTO error_log_table ...
    END IF;
END;--CADA QUE HAY UN ERROR DE SERVIDOR ORA-00942, SI QUITO EL IF, ENTRA EN CUALQUIER ERROR 


--**********************************++
--          DEPENDENCIAS            --
--**********************************++
SELECT name, type, referenced_name, referenced_type
FROM user_dependencies;


ALTER SYSTEM SET REMOTE_DEPENDENCIES_MODE =
{SIGNATURE | TIMESTAMP} --modificar para el trato en dependecias remotas en systema

ALTER SESSION SET REMOTE_DEPENDENCIES_MODE =
{SIGNATURE | TIMESTAMP} --modificar para el trato en dependecias remotas en la sesion actual


--**********************************++
--   PARAMETROS DE INICIALIZACION   --
--**********************************++
ALTER SESSION PLSQL_CODE_TYPE = NATIVE; --CODIGO MAQUINA
ALTER SESSION PLSQL_CODE_TYPE = INTERPRETED; --BYTECODE
ALTER SESSION PLSQL_OPTIMIZE_LEVEL = 0; --M�S LENTO
ALTER SESSION PLSQL_OPTIMIZE_LEVEL = 3; --MAS RAPIDO

--**********************************++
--  MENSAJES WARNING DEL COMPILADOR --
--**********************************++
CREATE OR REPLACE PROCEDURE count_emps IS
    v_count PLS_INTEGER;
BEGIN
    SELECT COUNT(*) INTO v_count FROM EMP;
    DBMS_OUTPUT.PUT_LINE(v_counter);
END; --lanza un ERROR porque la variable no est� declarada (v_counter)

CREATE OR REPLACE PROCEDURE unreachable IS
    c_const CONSTANT BOOLEAN := TRUE;
BEGIN
    IF c_const THEN
        DBMS_OUTPUT.PUT_LINE('TRUE');
    ELSE
        DBMS_OUTPUT.PUT_LINE('NOT TRUE');
    END IF;
END unreachable; --TIENE UNA RAMA INALCANZABBLE PERO COMPILA BIEN D:


ALTER SESSION SET PLSQL_WARNINGS = 'ENABLE:ALL'; --PARA QUE SE MUESTREN LOS WARNINGS :D
ALTER SESSION SET PLSQL_WARNINGS = 'DISABLE:ALL'; 
--O CON DBMS
BEGIN
    DBMS_WARNING.ADD_WARNING_SETTING_CAT
    ('PERFORMANCE','ENABLE','SESSION');
END; --ACTIVA PERFORMANCE EN SESSION (ADD)

BEGIN
    DBMS_WARNING.SET_WARNING_SETTING_STRING
    ('ENABLE:SEVERE','SESSION');
END;--DESACTIVA TODAS (SET) Y ACTIVA SEVERE EN SESSION

DECLARE
    v_string VARCHAR2(200);
BEGIN
    v_string :=
    DBMS_WARNING.GET_WARNING_SETTING_STRING;
    DBMS_OUTPUT.PUT_LINE(v_string);
END; --IMPRIMER LA CONFIGURACION DE WARNINGS (GET)

--*****************************
-- COMPILACION CONDICIONAL   --
--*****************************
CREATE OR REPLACE PROCEDURE lets_pretend IS --SI LA VERSION DE OFFICE ES  MAYOR A 2018 XD
BEGIN
    ...
    $IF MS_OFFICE_VERSION >= '2018' $THEN
        include_holographics;
    $ELSE
        include_std_graphic;
    $END
    ...
END lets_pretend;


CREATE OR REPLACE FUNCTION lets_be_real --SI LA VERSION ORACLE ES 11 O SUP, ENTONCES LA HACE DETERMINISTICA
    RETURN NUMBER
    $IF DBMS_DB_VERSION.VERSION >= 11 $THEN
        DETERMINISTIC
    $END
IS BEGIN
    RETURN 17; -- real source code here !
END lets_be_real;

BEGIN
    DBMS_OUTPUT.PUT_LINE('Function returned ' || lets_be_real);
END lets_be_real;

BEGIN
    DBMS_PREPROCESSOR.PRINT_POST_PROCESSED_SOURCE
    ('FUNCTION','BECAEVERIS','lets_be_real'); --OBJECT TYPE, SCHEMA OR USER, OBJECT NAME
END;  --PARA VER EL CODIGO QUE HA SIDO COMPILADO


--PLSQL-CCFLAGS
ALTER SESSION SET PLSQL_CCFLAGS = 'debug:true'; --PARA ACTIVARLAS  O CREAR EL VALOR 
ALTER SESSION SET PLSQL_CCFLAGS = 'debug:FALSE';

CREATE OR REPLACE PROCEDURE testproc IS BEGIN
    $IF $$debug $THEN
        DBMS_OUTPUT.PUT_LINE('This code was executed');
    $ELSE
        DBMS_OUTPUT.PUT_LINE('This code was NOT executed');
    $END
END testproc;

EXEC testproc;

--OTRO EJEMPLO
ALTER SESSION SET PLSQL_CCFLAGS = 'testflag:1';

CREATE OR REPLACE PROCEDURE testproc IS BEGIN
    $IF $$testflag > 0 $THEN
        DBMS_OUTPUT.PUT_LINE('This code was executed');
    $END
END testproc;

EXEC testproc;

BEGIN
    DBMS_PREPROCESSOR.PRINT_POST_PROCESSED_SOURCE
    ('PROCEDURE','BECAEVERIS','TESTPROC');
END; --PARA VER EL CODIGO :D


--************************************
--        HIDING SOURCE CODE        --
--************************************

--USING DBMS_DDL.CREATE_WRAPPED:
BEGIN
    DBMS_DDL.CREATE_WRAPPED
    ('CREATE OR REPLACE PROCEDURE mycleverproc
    (p_param1 IN NUMBER, p_param2 OUT NUMBER)
    IS BEGIN
     /* some clever but private code here */
    END mycleverproc;');
END; --ASI OTROS USUARIOS CON PRIVILEGIO DE EXECUTE SOLO PODRAN VER CODIGO HASHEADO
        --IN FACT, YO TAMPOCO PODR� VERLO, SO.... HAY QUE GUARDAR UNA COPIA DEL CODIGO XD
        
        
--TAMBIEN SE PUEDE ASINAR A UN VARCHAR2 Y LUEGO EJECUTAR EL WRAP
DECLARE
    v_code VARCHAR2(32767);
BEGIN
    v_code := 'CREATE OR REPLACE FUNCTION myclevererfunc '
    || '(p_param1 IN NUMBER) ' || 'RETURN NUMBER IS BEGIN '
    || '... /* some even cleverer but private code
    */ '
    || ' END myclevererfunc;';
    DBMS_DDL.CREATE_WRAPPED(v_code);
END;















