--Bloque anonimo para asignar permisos a un rol sobre las tablas del usuario actual

BEGIN
    FOR c IN (SELECT * FROM USER_TABLES)LOOP
        EXECUTE IMMEDIATE 'GRANT SELECT ON '||c.TABLE_NAME||' TO MI_ROL';
    END LOOP;
END;

--Emplea un cursor para recorrer las tablas del usuario actual
--Para que no se precompile la instrucción sql al crear el bloque, se utiliza EXECUTE IMMEDIATE
--De esta manera se compila hasta tener los parámetros requeridos.